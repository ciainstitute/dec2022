package main

import "fmt"

func main() {

	nums := [...]int{1, 5, 7, 9, 8}

	fmt.Println(nums[2])

	sum := 0

	for _, value := range nums {
		// for key, value := range nums { You can skip key by using _
		sum = sum + value
		// fmt.Printf("nums[%v] = %v\n", key, value)

	}

	fmt.Printf("Total: %v\n", sum)

	// var num int
	// fmt.Print("Enter number: ")
	// fmt.Scanf("%d", &num)

	// for i := 1; i <= 10; i++ {

	// 	fmt.Printf("%v * %v = %v\n", num, i, num*i)
	// }

}

// WAP to read a number and print its table e.g. 5 * 1 = 5 ... 5 * 10 = 50
