package main

import "fmt"

func main(){

	var length int
	var breadth int

	fmt.Print("Enter length: ")
	fmt.Scanf("%d", &length)

	fmt.Print("Enter breadth: ")
	fmt.Scanf("%d", &breadth)

	area := length * breadth

	fmt.Printf("Area of rectangle is %v\n", area)

}
