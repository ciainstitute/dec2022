package main;

import "fmt"

func main() {

	fmt.Printf("%v %T\n", 5, 5)
	fmt.Printf("%v %T\n", 5.5, 5.5)
	fmt.Printf("%v %T\n", "5", "5")
	fmt.Printf("%v %T\n", true, true)
}
