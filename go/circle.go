package main

import "fmt"

func main(){

	const PI = 3.14
	var radius float32

	fmt.Print("Enter radius: ")
	fmt.Scanf("%f", &radius)

	area := PI * radius * radius
	circ := 2 * PI * radius


	fmt.Printf("Area of circle is %v\nCircumference of circle is %v\n", area, circ)

}
