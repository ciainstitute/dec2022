package main

import "fmt"

type User struct {
	name string
	age  int
	city string
}

// CW WAP to print average salry from array of 3 employees
type Emp struct {
	name   string
	salary int
}

func printEmp(e Emp) {
	fmt.Println(e)
}

func main() {

	var emps = [3]Emp{Emp{"shailesh", 1000}, Emp{"priyanka", 2000}, Emp{"yogini", 3000}}

	var total_sal int
	var avg_sal int

	for _, e := range emps {
		printEmp(e)
		total_sal = total_sal + e.salary
	}

	avg_sal = total_sal / len(emps)

	fmt.Println(total_sal, avg_sal)

	// p2 := User{"Mrunal", 21, "Pune"}
	// fmt.Println(p2)

	// p3 := User{city: "Nagpur", age: 24, name: "Shailesh"}
	// fmt.Println(p3)

	// var num int
	// num = 5
	// fmt.Println(num)

	// var p1 User
	// p1.name = "Yogini"
	// p1.age = 20
	// p1.city = "Mumbai"
	// fmt.Println(p1)
	// fmt.Println(p1.city)

}
