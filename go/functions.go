package main

import "fmt"

func greet(name string) {
	fmt.Println("Hello ", name)
}

func getSqr(num int) int {
	return num * num
}

func getSqrCube(num int) (int, int) {
	return num * num, num * num * num
}

// WAF calc to return add, sub, mul, div of 2 numbers
func calc(x int, y int) (int, int, int, int) {
	add := x + y
	sub := x - y
	mul := x * y
	div := x / y

	return add, sub, mul, div
}

func main() {

	fmt.Println(calc(8, 4))

	sqr, cub := getSqrCube(5)
	fmt.Println(sqr, cub)

	fmt.Println(getSqr(5))
	greet("Priyanka")
	greet("Yogini")
	greet("Mrunal")
}
