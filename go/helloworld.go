package main

import "fmt"

func main() {

	var num int;
	fmt.Print("Enter a number: ")
	fmt.Scanf("%d", &num)
	fmt.Println(num);
	fmt.Printf("Type of num is %T\n", num)
	fmt.Printf("Square of %v is %v\n", num, num * num)


	num1 := 5;
	num2 := 6;

	fmt.Printf("%v + %v = %v\n", num1, num2, num1 + num2)	

}
