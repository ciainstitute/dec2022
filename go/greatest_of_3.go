//WAP to read 3 numbers and find greatest among them

package main

import "fmt"

func main() {
	var num1 int
	var num2 int
	var num3 int

	fmt.Print("Enter num1: ")
	fmt.Scanf("%d", &num1)

	fmt.Print("Enter num2: ")
	fmt.Scanf("%d", &num2)

	fmt.Print("Enter num3: ")
	fmt.Scanf("%d", &num3)

	if num1 > num2 && num1 > num3 {
		fmt.Printf("%v is greatest\n", num1)
	} else if num2 > num3 {
		fmt.Printf("%v is greatest\n", num2)
	} else {
		fmt.Printf("%v is greatest\n", num3)
	}

}
