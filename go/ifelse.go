package main

import "fmt"

func main() {

	var num int
	fmt.Print("Enter number: ")
	fmt.Scanf("%d", &num)

	if num > 0 {
		fmt.Printf("%v is +ve\n", num)
	} else {
		fmt.Printf("%v is -ve\n", num)
	}

}
