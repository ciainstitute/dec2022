//WAP to read character and print its color (r,g,b) use switch case

package main

import "fmt"

func main() {
	var color_code string

	fmt.Print("Enter color code (r/g/b): ")
	fmt.Scan(&color_code)

	switch color_code {
	case "r", "R":
		fmt.Println("The color is red")
	case "g", "G":
		fmt.Println("The color is green")
	case "b", "B":
		fmt.Println("The color is blue")
	default:
		fmt.Println("Invalid color code")
	}
}
