package main

import "fmt"

func main() {

	nums := []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}

	nums2 := nums[:5]

	// copy(nums2, nums)

	fmt.Println(nums2)

	// nums := make([]int, 3, 5)

	// fmt.Println("Length: ", len(nums))
	// fmt.Println("Cap: ", cap(nums))

	// nums[0] = 1
	// nums[1] = 1
	// nums[2] = 1

	// nums = append(nums, 4, 5, 6, 7)

	// fmt.Println(nums)

	// nums2 := append(nums, []int{11, 12, 13}...)

	// fmt.Println(nums2)

	//WAP to find even and odd numbers from array and store in evenarr and oddarr

	// nums := [...]int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}

	// evenarr := []int{}

	// oddarr := []int{}

	// fmt.Println(nums, evenarr, oddarr)

	// for _, val := range nums {
	// 	if val%2 == 0 {

	// 		evenarr = append(evenarr, val)
	// 	} else {
	// 		oddarr = append(oddarr, val)
	// 	}
	// }

	// fmt.Println("Even Array", evenarr)
	// fmt.Println("Odd Array", oddarr)

	// slice1 := []int{1, 3, 4, 6, 8}

	// fmt.Println(slice1)

	// // slice1 = append(slice1, 9, 10)
	// slice1 = slice1[3:]

	// for key, val := range slice1 {
	// 	fmt.Printf("slice1[%v] = %v\n", key, val)
	// }

}
