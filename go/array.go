package main

import "fmt"

func main() {

	//WAP to find even and odd numbers from array of 10 elements and put it in separate arrays nums, even_arr push/append, odd_arr push/append

	//WAP to read a number and search the same in array if found then print position as well

	nums := [...]int{2, 4, 6, 8, 10}
	var search int

	fmt.Print("Search a number: ")
	fmt.Scan(&search)

	flag := false

	for key, value := range nums {
		if search == value {
			flag = true
			fmt.Printf("%v is found at %v\n", search, key+1)
			break
		}
	}

	if flag == false {
		fmt.Printf("%v not found in array\n", search)
	}

	// fmt.Println(nums[2])
	// fmt.Println(len(nums))

	// for i := 0; i < len(nums); i++ {
	// 	fmt.Printf("nums[%v] = %v\n", i, nums[i])
	// }

	// for key, value := range nums {
	// 	fmt.Printf("%v => %v\n", key, value)
	// }

	// var arr1 = [5]int{1, 5, 7, 9, 3}
	// var arr2 = [...]int{2, 4, 6}
	// var arr3 = [5]int{2, 8}
	// var arr4 = [5]int{1: 5, 3: 7}

	// fmt.Println(arr1)
	// fmt.Println(arr2)
	// fmt.Println(arr3)
	// fmt.Println(arr4)

}
