#!/usr/bin/perl
=basic
#print @ARGV;

my $name = $ARGV[0];

my @arr = @ARGV;

print "Hello $arr[0]\n";

=cut

@fruits = ('apple', 'banana', 'chiku');

print $fruits[1];

%rec = ("name" => "Shailesh", "age" => 23, "city" => "Nagpur");

print $rec{"name"};

print "\n";
print @fruits;
print "\n";

print %rec;
print "\n";

@keysofrec = keys %rec;
print @keysofrec;
print "\n";

use Data::Dumper;
print Dumper(\@fruits);
print Dumper(\%rec);
